﻿using ProductLibrary;

task1 productObj = new task1();
productObj.product_id = 323;
productObj.product_name = "mobile";
productObj.product_price = 45000;
productObj.rating = 3;

Console.WriteLine($"Product Id ::{productObj.product_id}\t Product Name::{productObj.product_name}\t Product Price::{productObj.product_price}\t Product Rating::{productObj.rating}");

//calling method

Console.WriteLine("Searching Product Here");
Console.Write("Enter Product Name :");
string? product_name = Console.ReadLine();
Console.Write("Enter Rating of Product:");
float rating = float.Parse(Console.ReadLine());
productObj.SearchProduct(product_name, rating);

