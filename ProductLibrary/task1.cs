﻿namespace ProductLibrary
{
    public class task1
    {
        //field
        public int product_id;
        public string product_name;
        public int product_price;
        public int rating;

        //Method
        public void SearchProduct(string product_name,float rating)
        {
            if(product_name == "laptop" && rating == 4.5)
            {
                Console.WriteLine("Product is abailable");
            }
            else
            {
                Console.WriteLine("Sorry...\nProduct is not abailable");
            }
        }
    }
}